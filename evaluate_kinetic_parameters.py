import numpy as np
from build_shape import ShapelBuilder
from math import *

import pandas as pd

class KineticFeatureBuilder:

    @staticmethod
    def get_points_from_cluster(data, cluster_num):

        cluster = data[:, 4]
        data = data[np.where(cluster == cluster_num)]
        x = data[:, 0]
        y = data[:, 1]
        t = data[:, 2]
        p = data[:, 3]
        # cluster = data[:, 4]
        points = np.vstack((t, x, y, p)).T

        return points

    @staticmethod
    def process_abs_isfinite(np_arr):

        return np.absolute(np_arr[np.isfinite(np_arr)])

    @staticmethod
    def evaluate(points):

        dist_arr = [0]
        time_arr = [0]
        velo_arr = [0]
        acc_arr = [0]
        slope_arr = [0]
        slope_dif_arr = [0]
        press_dif_arr = [0]
        jerk_arr = [0]

        x = points[:, 1]
        y = points[:, 2]
        t = points[:, 0]
        p = points[:, 3]

        rows = len(x)

        for i in range (1, rows):
            # Euclidean distance
            dist_arr.append(sqrt((x[i] - x[i-1])**2 + (y[i]-y[i-1])**2))

            # Length in seconds
            time_arr.append((t[i] - t[i-1]) / 1000.0)

            # Velocity, Acceleration and other kinematic parameters
            velo_arr.append(dist_arr[i] / time_arr[i])
            acc_arr.append((velo_arr[i] - velo_arr[i-1])/time_arr[i])
            slope_arr.append((y[i] - y[i-1]) / (x[i] - x[i-1]))
            slope_dif_arr.append(slope_arr[i] - slope_arr[i-1])

            press_dif_arr.append(p[i])
            # press_dif_arr.append(p[i] - p[i - 1])

            jerk_arr.append((acc_arr[i] - acc_arr[i-1]) / time_arr[i])

        # # Testing
        # print(time_arr)
        # print(velo_arr)
        # print(acc_arr)
        # print(slope_arr)
        # print(slope_dif_arr)
        # print(press_dif_arr)
        # print(jerk_arr)

        velo_arr = KineticFeatureBuilder.process_abs_isfinite(np.asarray(velo_arr))
        acc_arr = KineticFeatureBuilder.process_abs_isfinite(np.asarray(acc_arr))
        slope_arr = KineticFeatureBuilder.process_abs_isfinite(np.asarray(slope_arr))
        slope_dif_arr = KineticFeatureBuilder.process_abs_isfinite(np.asarray(slope_dif_arr))
        press_dif_arr = KineticFeatureBuilder.process_abs_isfinite(np.asarray(press_dif_arr))
        jerk_arr = KineticFeatureBuilder.process_abs_isfinite(np.asarray(jerk_arr))

        # value.velo = velo(isfinite(velo));
        # value.slope = slope(isfinite(slope));
        # value.acc = acc(isfinite(acc));
        # value.jerk = jerk(isfinite(jerk));

        res_trajlength = np.sum(dist_arr)
        res_timeinterval = (t[0] - t[rows-1]) / 1000.0

        res_velocity_mass = np.nansum(velo_arr)
        res_acc_mass = np.nansum(acc_arr)
        res_press_dif = np.nansum(press_dif_arr)
        res_angle_mass = np.nansum(slope_dif_arr)
        res_jerkmass = np.nansum(jerk_arr)

        res_avg_velo = np.mean(velo_arr)
        res_avg_acc = np.mean(acc_arr)
        res_avg_slope = np.mean(slope_arr)
        res_avg_slope_dif = np.mean(slope_dif_arr)
        res_avg_press_dif = np.mean(press_dif_arr)
        res_avg_jerk = np.mean(jerk_arr)

        # print("res_trajlength:      %.4f mm"        % res_trajlength)
        # print("res_timeinterval:    %.4f sec"       % res_timeinterval)
        # print("res_velocity_mass:   %.4f"           % res_velocity_mass)
        # print("res_acc_mass:        %.4f"           % res_acc_mass)
        # print("res_press_dif:       %.4f"           % res_press_dif)
        # print("res_angle_mass:      %.4f"           % res_angle_mass)
        # print("res_jerkmass:        %.4f"           % res_jerkmass)

        # print("res_avg_velo:        %.4f"           % res_avg_velo)
        # print("res_avg_acc:         %.4f"           % res_avg_acc)
        # print("res_avg_slope:       %.4f"           % res_avg_slope)
        # print("res_avg_slope_dif:   %.4f"           % res_avg_slope_dif)
        # print("res_avg_press_dif:   %.4f"           % res_avg_press_dif)
        # print("res_avg_jerk:        %.4f"           % res_avg_jerk)

        result = [
            res_trajlength,
            res_timeinterval,
            res_velocity_mass,
            res_acc_mass,
            res_press_dif,
            res_angle_mass,
            res_jerkmass,
            res_avg_velo,
            res_avg_acc,
            res_avg_slope,
            res_avg_slope_dif,
            res_avg_press_dif,
            res_avg_jerk]

        return result

    @staticmethod
    def get_features_from_shape(shape, shape_orig):

        # Load shape
        # shape = ShapelBuilder.get_single_test_csv_shape("./ShapeRecordedCSV/","fork_test.csv")

        data = shape.data



        clusters = data[:, 4]

        # print('shape_data')
        # print(data)

        cluster_list = np.unique(clusters)

        names = [
            "res_trajlength",
            "res_timeinterval",
            "res_velocity_mass",
            "res_acc_mass",
            "res_press_dif",
            "res_angle_mass",
            "res_jerkmass",
            "res_avg_velo",
            "res_avg_acc",
            "res_avg_slope",
            "res_avg_slope_dif",
            "res_avg_press_dif",
            "res_avg_jerk",
            "centr_x",
            "centr_y",
            "err"]

        result = []

        for i in cluster_list:

            row = []
            points = KineticFeatureBuilder.get_points_from_cluster(data, int(i))
            row = KineticFeatureBuilder.evaluate(points)

            # Calculating additioanl features for each cluster

            # Centroids
            centr_x = shape_orig.centroids[int(i)][0]
            centr_y = shape_orig.centroids[int(i)][1]
            # print("%f %f" % (centr_x, centr_y))

            # Error label
            if "error" in shape.filename:
                err = 1
            else:
                err = 0

            row = row + [centr_x, centr_y, err]

            # print(row)
            result.append(row)

        result = np.asarray(result)
        result_pd = pd.DataFrame(data=result, columns=names)

        # print(result_np)
        # print(result_pd)

        return result

# # Testing
# # Load test shape
# shape = ShapelBuilder.get_single_test_csv_shape("./ShapeRecordedCSV/","fork_test.csv")
# features = KineticFeatureBuilder.get_features_from_shape(shape)
# print(features)