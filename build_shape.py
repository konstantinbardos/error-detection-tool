import os
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import pandas as pd
import json
import datetime

from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import KMeans
from sklearn.neighbors import KNeighborsClassifier
from sklearn import datasets

class ShapelBuilder:

    # Unit conversion methods
    def convert_pix_android_to_mm(data_frame):
        data_frame.X /= (149.82489 / 25.4)
        data_frame.Y /= (149.82489 / 25.4)

        # Reduce size to fit iPad Screen
        data_frame.X *= 0.95
        data_frame.Y *= 0.95
        return data_frame

    def convert_mm_to_pix_ios(data_frame):
        data_frame.X /= (25.4 / 264) * 2
        data_frame.Y /= (25.4 / 264) * 2
        return data_frame

    def convert_pix_ios_to_mm(data_frame):
        data_frame.X *= (25.4 / 264) * 2
        data_frame.Y *= (25.4 / 264) * 2
        return data_frame

    # Geometry aligning methods
    def calculate_offset(data_frame, width, height):
        min_x = min(data_frame.X)
        min_y = min(data_frame.Y)
        max_x = max(data_frame.X)
        max_y = max(data_frame.Y)
        offset_x = (max_x + min_x) / 2.0 - width / 2.0
        offset_y = (max_y + min_y) / 2.0 - height / 2.0
        return offset_x, offset_y

    def center_shape(data_frame, width, height):
        offset_x, offset_y = ShapelBuilder.calculate_offset(data_frame, width, height)
        data_frame.X -= offset_x
        data_frame.Y -= offset_y
        return data_frame

    # Print useful info to console
    def print_shape_params(shape):
        print("Shape:")
        print("---------------------")
        print("MaxX:    " + str(max(shape.X)))
        print("MinX:    " + str(min(shape.X)))
        print("---------------------")
        print("MaxY:    " + str(max(shape.Y)))
        print("MinY:    " + str(min(shape.Y)) + "\n")


    # Load shape from csv

    def get_single_original_csv_shape(path, file, variables=['x', 'y']):
        arr = pd.read_csv(path + file, names=variables).as_matrix()
        return Shape(arr, file)

    def get_single_test_csv_shape(path, file, variables=['x', 'y', 't', 'p', 'c']):
        df = pd.read_csv(path + file, names=variables)
        # df.y *= -1
        arr = df.as_matrix()
        return Shape(arr, file)

    def get_single_csv_shape(path, file, variables=['x', 'y', 't', 'p']):
        df = pd.read_csv(path + file, names=variables)
        df.y *= -1
        arr = df.as_matrix()
        return Shape(arr, file)

    def get_multiple_csv_shapes(path, variables=['x', 'y', 't', 'p']):
        arr_list = []
        for file in os.listdir(path):
            if file.endswith(".csv"):
                arr = ShapelBuilder.get_single_csv_shape(path, file, variables)
                arr_list.append(arr)
        return arr_list

class Shape:
    def __init__(self, arr, filename='unknown'):

        # print(filename)
        self.data = arr
        self.filename = filename
