from pandas import DataFrame, read_csv

import os
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib
matplotlib.style.use('ggplot')
import numpy as np
import json

Location = r'./ShapeEtalons/sine_trace.csv'
orig = pd.read_csv(Location, names=['X', 'Y'])

dir_collected = "./ShapeCollectedData/"
dir_etalons = "./ShapeEtalons/poppelreuter/"
dir_json = "./ShapeJSON/"

plt.figure(1)
plt.axis([0,1024, 0, 768])

# Convert from Android pix to mm

def print_shape_params(shape):
    print("Shape:")
    print("---------------------")
    print("MaxX:    " + str(max(shape.X)))
    print("MinX:    " + str(min(shape.X)))
    print("---------------------")
    print("MaxY:    " + str(max(shape.Y)))
    print("MinY:    " + str(min(shape.Y)) + "\n")

def convert_pix_android_to_mm(data_frame):
    data_frame.X /= (149.82489 / 25.4)
    data_frame.Y /= (149.82489 / 25.4)
    # Reduce size to fit iPad Screen
    data_frame.X *= 0.95
    # data_frame.Y *= 0.95
    return data_frame

def convert_mm_to_pix_ios(data_frame):
    data_frame.X /= (25.4 / 264.0) * 2.0
    data_frame.Y /= (25.4 / 264.0) * 2.0
    return data_frame

def calculate_offset(data_frame, width, height):
    min_x = min(data_frame.X)
    min_y = min(data_frame.Y)
    max_x = max(data_frame.X)
    max_y = max(data_frame.Y)
    offset_x = (max_x + min_x) / 2.0 - width / 2.0
    offset_y = (max_y + min_y) / 2.0 - height / 2.0
    return offset_x, offset_y

def center_shape(data_frame, width, height):
    offset_x, offset_y = calculate_offset(data_frame, width, height)
    data_frame.X -= offset_x
    data_frame.Y -= offset_y
    return data_frame

def convert_to_json(data_frame, name="none", folder="./"):
    data = []
    for index in range(0, len(data_frame.X)):
        x = data_frame.X[index]
        y = data_frame.Y[index]
        point = {"x": x,
                 "y": y,
                 "force": 0,
                 "aziang": 0,
                 "altang": 0,
                 "time": 0}
        data.append(point)
        # print(str(data_frame.X[index]) + " " + str(data_frame.Y[index]))

    txtfile = open(folder + name + ".json", "w")
    print(txtfile)
    shape_json = json.dump({"data": [data],
                            "id": name,
                            "time": "2016-10-10 10:10:10 +0000",
                            "patientId": name,
                            "type": name}, txtfile, sort_keys=True, indent=4)
    # print(shape_json)
    return shape_json


# orig = convert_pix_android_to_mm(orig)
# orig = convert_mm_to_pix_ios(orig)
# orig = center_shape(orig, 1024, 768)
# print_shape_params(orig)
# convert_to_json(orig, name="none")

# plt.scatter(orig.X, orig.Y)

df_combined = []

for file in os.listdir(dir_etalons):
    if file.endswith(".csv"):
        # plt.figure()

        shape_name = file[0:-4]
        print(dir_etalons + file)

        df = pd.read_csv(dir_etalons + file, names=['X', 'Y'])
        df = convert_pix_android_to_mm(df)
        df = convert_mm_to_pix_ios(df)
        # print_shape_params(df)

        offset_x, offset_y = calculate_offset(df, 1024, 768)
        df.X -= offset_x
        df.Y -= offset_y

        df.Y = df.Y * -1.0 + 786
        plt.scatter(df.X, df.Y)

        df_combined.append(df)

        # print("Length to add:", len(df.X))
        # print("Total length:", len(df_combined))

        # df.to_json(path_or_buf="ShapeJSON/"+ shape_name +".json")
        # convert_to_json(df, name=shape_name, folder=dir_json)

# print(df_combined)

convert_to_json(pd.concat(df_combined, ignore_index=True), name="poppelreuter", folder=dir_json)
plt.show()