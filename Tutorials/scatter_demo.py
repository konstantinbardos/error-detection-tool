"""
Simple demo of a scatter plot.
"""
import numpy as np
from numpy import ones
import matplotlib.pyplot as plt

N = 50
x = np.random.rand(N)
y = np.random.rand(N)

z = ones((1, 10)) * 2
z = ((x * 0) + 1) * 15

print(z)

colors = np.random.rand(N)
area = np.pi * (15 * np.random.rand(N))**2  # 0 to 15 point radiuses

plt.scatter(x, y, s=area, c=colors, alpha=0.5)
plt.show()

